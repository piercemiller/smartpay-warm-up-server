const port = 5000;
const companies = require('./info/companies.json');
const express = require('express');
const cors = require('cors');
const spProject = express();
/*Using Cors with Express
*https://medium.com/@alexishevia/using-cors-in-express-cac7e29b005b*/
spProject.use(cors());
spProject.get('/companies', (req, res) => res.json(companies))
spProject.listen(port, () => console.log('Warm Up Server: '+port))